# coding=utf-8
"""
#HOLAMUNDO

print "Hola Python"
print "Hola Python"
print "Hola Python"

"""

"""
#ENTEROS, REALES Y OPERADORES ARITMETICOS

e = 5 #tipo INT
e = 5L #tipo Long

#REALES
real = 0.567
real = 0.56e-3

#Operadores Aritmeticos
a = 26
b = 11.3
c = 5
d = 3.5

#SUMA
print a + b

#RESTA
print  c - a

#MULTIPLICACION
print d * c

#Exponente
print c ** 2

#DIVISION
print float(c) / a

#DIVISION ENTERA
print c / a

#MODULO
print  7%3

"""

"""
#BOLEANOS OPERADORES LOGICOS Y CADENAS

#COMILLAS SIMPLES
cadenaS = 'texto entre comillas simple'

#COMILLAS DOBLES
cadenaD = "texto entre \n\tcomillas dobles"

#print type(cadenaS)
#print  cadenaD


#REPETICION Y CONCATENACION
cad = "\ncadena" * 3
print  cad

cad1 = "cadena 1"
cad2 = " cadena 2"

print cad1 + cad2

#OPERADORES BOLEANOS
bt = True
bf = False

bAnd = True and False
bOr = True or False
bNot = not True

print bAnd
print bOr
print bNot
"""

"""
#LISTAS

l = [2,"tres",True,['Uno',10]]

l[1] = 4

l2 = l[1]

l3 = l[0:3:2]

print l3
"""

"""
#TUPLAS

t = (3,"Hola",False)

print  type(t)

print t[1]

#NO PUEDO MODIFICAR LOS ELEMENTOS DE UNA TUPLA
#t[0]  = "Hola"
"""

"""
#DICCIONARIOS

d = {
        'clave1':[1,2,3],
        'clave2':True,
        4:"Hola"
    }

d[4] = False

print  d[4]
"""

"""
#OPERADORES RELACIONALES

v = "Hola"
c = "Hola"

r = c == v

print r
"""

"""
#SENTENCIAS CONDICIONALES

edad = 18
m_edad = 18

if edad >= m_edad:
    print "Eres mayor de edad"
    if False:
       print "Esto se ejecuta siempre que sea mayor de edad"
    else:
         print "Cualquier cosa"

else:
    print "No eres mayor de edad"


#ELSE IF

edad = 2

if edad >= 0 and edad < 18:
    print "Eres un niño"
elif edad >= 18 and edad < 27:
    print "Eres un joven"
elif edad >=27 and edad < 60:
    print "Eres un adulto"
else:
    print "Eres de la tercera edad"

"""

"""
#BUCLES

edad = 0

while edad <= 20:

    if edad == 15:
        edad = edad + 1
        break

    print "Tienes: " + str(edad)
    edad = edad + 1



lista = ['Elemento 1','Elemento 2','Elemento 3']

for elem in lista:
    print elem

print "\n"


for letra in "Mario":
    print letra


"""

"""

#FUNCIONES

def mi_function(num1,num2):
    return  num1 + num2

resultado_suma = mi_function(3,4)

print  resultado_suma
"""

"""

#CLASES Y OBJETOS


class Humano:
    def __init__(self,Edad):
        self.edad = Edad


    def hablar(self,mensaje):
        print  self.edad
        print  mensaje


pedro = Humano(26)
raul = Humano(21)

print 'Soy Pedro y tengo',pedro.edad
print  'soy Raul y tengo',raul.edad

pedro.hablar('Hola')
raul.hablar('Hola Pedro!')

"""

"""

#HERENCIA SIMPLE

class Humano:
    def __init__(self,Edad):
        self.edad = Edad


    def hablar(self,mensaje):
        print  mensaje



class IngSistemas(Humano):
    def __init__(self):
        print "Hola"

    def programar(self,lenguaje):
        print "Voy a programar en",lenguaje


class LicDerecho(Humano):
    def estudiarCaso(self,de):
        print 'debo estudiar el caso de', de



pedro = IngSistemas()
raul = LicDerecho(21)

pedro.programar('Python')
raul.estudiarCaso('Pedro')

pedro.hablar('Hola')
raul.hablar('Hola Pedro!')

"""

"""

#DICCIONARIOS

diccionario = {

    "redes_sociales":['Twitter','Facebook','LinkedIn'],
    3:'Tres',
    'Hola':"Mundo"
}

diccionario.has_key('Hola')
diccionario.items()
diccionario.keys()
diccionario.values()



#print diccionario.pop(3,'Clave no encontrada')
#del diccionario[3]
#diccionario.clear()
#diccionario['Clave_nueva'] = "NuevoValor"

diccionario_2 = diccionario.copy()

print diccionario
print diccionario_2

"""

"""

#ENCAPSULACION

#Al inciar un metodo con dos guiones bajos "__" python lo reconoce como un metodo privado no debe terminar con dos guines bajos

class Prueba(object):
    def __init__(self):
        self.__Privado = "Soy Privado"
        self.Privado = "Soy Publico"

    def __metodoPrivado(self):
        return "Soy Metodo Privado"

    def metodoPublico(self):
        print "Soy Metodo Publico"

    def getPrivado(self):
        return self.__Privado

    def setPrivado(self,valor):
        self.__Privado = self.__metodoPrivado()

obj = Prueba()


obj.setPrivado("Tengo nuevo Valor")
print obj.getPrivado()

#obj.metodoPublico()
#obj.__metodoPrivado()

"""

"""

#FUNCIONES DE ORDEN SUPERIOR


def Prueba(f):
    return f()

def porEnviar():
    return 2 + 2

print Prueba(porEnviar)


def seleccion(ops):

    def suma(n,m):
        return "La suma es: " +  str(n + m)

    def multiplicacion(n,m):
        return "La multiplicacion es: " + str(n * m)

    if ops == 'suma':
        return suma
    elif ops == 'multi':
        return multiplicacion



fGuardada = seleccion("multi")
print  fGuardada(5,5)

"""

"""

#FUNCION MAP

def operador(n,m):

    if n == None or m == None:
        return 0

    return n+m

l1 = [1,2,3,4]
t1 = (9,8,7)

lr = map(operador,l1,t1)

print l1
print t1
print lr

"""

"""

#EJEMPLO DE SACAR EL CODIGO ASCII

t = 0
for i in range(ord('a'), ord('z')+1):
    t = t + i
    print i
print "El total de los codigos ASCII es: " + str(t)
"""

"""

#FUNCION FILTER

def filtro(elem):
    return (elem == "o")

l = [1,-3,2,-7,-8,-9,10]
s = "Hola Mundo"

lr = filter(filtro,s)

print s
print type(lr)
"""

"""

#FUNCION REDUCE

s = ('H','O','L','A','_','M','U','N','D','O')
n = (1,2,3)


def concatenar(a,b):
    return a + b

def suma(a,b):
    return a + b


sr = reduce(suma,n)

print type(sr)
print sr

"""
