<?php

//OPERADORES

/*Sieste tipos de opetadores en PHP
	
	1.Aritmetica
	2.Asignacion
	3.Cadena
	4.Comparacion
	5.Errores
	6.Incremento
	7.Logica
*/
	/*OPERADORES DE ARITMETICA
		
		1.Adicion(+)
		2.Substraccion(-)
		3.Multiplicacion(*)
		4.Division(/)
		5.Porcentaje(%)
	*/

		//Ejemplos
		$adicion = 4 + 4;
		$multi = $adicion * 3;

		//echo $adicion;
		//echo "<br/>";
		//echo $multi;

	/*OPERADORES DE ASIGNACION

		Asigancion(=)
	*/
		//Ejemplos
		$valor1 = "Hola";
		$valor2 = 20;

		//echo $valor1;
		//echo "<br/>";
		//echo $valor2;

	/*OPERADOR DE CADENA O CONCATENACION
		
		1.Concatenacion(.)
	*/
		/*Ejemplos
		$texto1 = "Hola";
		$texto2 = "Soy";
		$texto3 = "Mario";

		echo $texto1.$texto2.$texto3;
		echo "<br/>";

		$tengo = "Yo ";
		$tengo .= "tengo ";
		$tengo .= "Nuevo ";
		$tengo .= "Carro";

		echo $tengo;
		*/

	/*OPERADORES DE COMPARACION
		
		1.Igual(==)
		2.Identico(===)
		3.Diferente(!= o <>)
		4.No Identicos(!==)
		5.Menor que(<)
		6.Mayor que(>)
		7.Menor o igual que(<=)
		8.Mayor o igual que(>=)
	*/
		//Ejemplos
		//echo (6 >= 4);
		//echo (5 == 11);

	/*OPERADORES DE CONTROL DE ERRORES

		1.Arroba(@)
	*/
		//Ejemplos
		//echo 'Hola';
		@ 
		define();

	/*OPERADORES DE INCREMENTO

		1.Incremento(++)
	*/
		//Ejemplos
		$Identity = 3;
		//echo ++$Identity;
		//echo "<br/>";
		//echo ++$Identity;

	/*OPERADORES DE LOGICA

		1.Y (&& o and) => Devuelve 1 si ambos operandos son 1
		2.O (|| o or) => Devuelve 1 si alguno de los dos operandos es 1
		3.O Exclusivo (xor) => Devuelve 1 si solo un operando es 1
	*/
		//Ejemplos
		echo ((2 == 2) && (2 != 1));
		echo "<br/>";
		echo ((3 == 3) or (4 != 4));


?>