<?php

//Comentarios y Variables

/*
Comentario de varias lineas
*/

//Las variables se define con el simbolo de $

/*
$valor = 50;
$cadena = "<br/>Mario Galindo";
$bool = true;

echo $valor;
echo $cadena;
echo $bool;
*/

//CONSTANTES y CONCATENACION
//Se llaman y se definen de manera diferente

define(saludo, "Hola como estan?"); //Constante
//echo saludo;

define(numero,20);
//echo numero;

//CONCATENACION
//echo saludo."&nbsp tengo &nbsp;".numero."&nbsp;anos";


//ARREGLOS O VECTORES
/*
	1.Arrays Predefinidos
	2.Arrays Asociativos o personalizados
*/

//ARRAYS PREDEFINIDOS
	$matrix = array("Prueba de Elemento 1",3,"Elemento 3");
	//echo $matrix[1];


//ARRAYS ASOCIATIVOS
	$arreglo_Asociativo = array("Clave_1" => "Prueba Asociativo","Clave_2" => 30);
	//echo $arreglo_Asociativo[Clave_2];


//Test
	$figuras = array();
	//echo $figuras[0];
	//array_unshift($figuras, "Agua");
	$figuras[] = "Hierro";
 	echo $figuras[0];

 	echo count($figuras);




?>