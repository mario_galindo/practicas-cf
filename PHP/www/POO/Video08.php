<?php

	//Interfaces de Objeto

		Interface Automovil{

			//Los metodos siempre deben de ser publicos
				public function encender();
				public function apagar();
		}

		Interface gasolina extends Automovil{

			public function vaciaTanque();
			public function llenarTanque($cantidad);
		}




			class deportivo implements gasolina{

				//Atributos
				private $estado = false;
				private $tanque = 0;

					//Metodos

					public function estado(){
						
						if($this->estado)
							print "El auto esta encedido y tiene ".$this->tanque." de litros en el tanque <br>";
						else
							print "El auto esta apagado <br>";

					}

					public function encender(){

							if($this->estado){

								print "No puedes encender el auto 2 veces";
							}
							else{

								if($this->tanque <= 0){
									print "No puede encender el carro por que el tanque esta vacio <br>";
								}else{

									print "Auto encendido <br>";
									$this->estado = true;	
								}

							}
								
					}

					public function apagar(){

						if($this->estado){

								print "Auto apagado <br>";
								$this->estado = false;
							}
							else{

								print "Auto ya esta apagado <br>";
								
							}

					}

					public function vaciaTanque(){

						$this->tanque = 0;
					}

					public function llenarTanque($canti){

							$this->tanque = $canti;
					}

					public function usar($km){

						if ($this->estado) {
							
							$reducir = $km / 3; 
							$this->tanque = $this->tanque - $reducir; 

								if($this->tanque <= 0)
									$this->estado = false;

						}else{
							print "El auto esta apagado y no se puede usar <br>";
						}
					}
			}


		$carro = new deportivo();
		$carro->llenarTanque(100);
		$carro->encender();
		$carro->usar(350);
		$carro->estado();


?>