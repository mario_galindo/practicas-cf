<?php

	//Metodos y atributos estaticos

		class pagina{

			//Atributos
			public $nombre = "Codigo facilito";
			public static $url = "www.codigofacilito.com";

			//Metodos

			public function bienvenida(){

				echo "Bienvenidos a <b>:".$this->nombre."</b> la pagina es: "."<b>".self::$url; //Acceder a atributos estaticos dentro de la clase "self::$url"
			}

			public static function bienvenida2(){

				echo "Bienvenidos a: ";
			}
		}


		class web extends pagina{


		}


		$pag = new pagina();
		//$pag->bienvenida();

		web::bienvenida2();

?>