<?php

	//Metodos _construct & __destruct

		class loteria{


			//Atributos
			public $numero;
			public $intentos;
			public $resultado = false;


			//Constructor
			public function __construct($num,$inten){

				$this->numero = $num;
				$this->intentos = $inten;
			}

			
			//Metodos

			public function sortear(){

				$minimo = $this->numero/2;
				$maximo = $this->numero*2;

					for ($i=0; $i < $this->intentos ; $i++) { 
						$intent = rand($minimo,$maximo);
						self::intentos($intent);
					}
			}

			public function intentos($intento){

				if ($intento == $this->numero) {
					 
					 echo "<b>".$intento ." == ".$this->numero."</b><br>";
					 $this->resultado = true;
				}else{

					echo $intento." != ".$this->numero."<br>";
				}
			}

			

			//Destructor
			public function __destruct(){

				if ($this->resultado) {
					
					echo "Felicidades, has acertado en ".$this->intentos." intentos";
				}else{

					echo "Que lastima, has perdido en ".$this->intentos." intentos";
				}
			}


		}


		$loteria = new loteria(10,5);
		$loteria->sortear();


?>