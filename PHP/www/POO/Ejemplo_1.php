<?php



	class persona{

		//Atributos

		public $Nombre = array();
		public $apellido = array();
		

		//Metodos
		public function guardar($name,$lastname){

			$this->Nombre[] = $name; //Al usar this no se usa el "$" incorrecto = "this->$nombre;" correcto = "this->nombre; 'estamos dentro de la clase'"
			$this->apellido[] = $lastname;
			
		}


		public function mostrar(){

			for ($i=0; $i < count($this->Nombre) ; $i++) { 
		 		
		 		self::formato($this->Nombre[$i],$this->apellido[$i]);
		 		//Otra Opcion
		 		//persona::formato($this->Nombre[$i],$this->apellido[$i]);
			}
		}


		public function formato($nom,$apell){

			echo "Nombre: ".$nom . " | Apellido: ".$apell."<br/>";
		}

	}



	
	$persona1 = new persona();
	$persona1->guardar("Mario","Galindo");
	$persona1->guardar("Juan","Guevara");
	$persona1->guardar("Miguel","Lagos");
	$persona1->mostrar();
	

?>