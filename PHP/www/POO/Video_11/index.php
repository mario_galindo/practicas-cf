
<?php


	//Uso de Traits

		trait Persona2{

			public $nombre;

			public function mostrarNombre(){
				
				echo $this->nombre;				
			}


			abstract function asignarNombre($name);

		}



		class Persona{

			use Persona2;

				public function asignarNombre($name){

					$this->nombre = $name;
				}
		}

		$persona = new Persona();
		$persona->asignarNombre("Mario");
		echo $persona->nombre;

?>