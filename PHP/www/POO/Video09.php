<?php

	//Clases y metodos abstractos

		abstract class Molde{

			abstract public function ingresarNombre($nombre);
			abstract public function obtenerNombre();
		}


			class persona extends Molde{

				private $mensaje = "Hola gente de codigo facilito";
				private $nombre;


				public function mostrar(){

					print $this->mensaje;
				}

				public function ingresarNombre($nombre){

					$this->nombre = $nombre;
				}


				public function obtenerNombre(){

					print $this->nombre;
				}
			}


			$per = new persona();
			$per->ingresarNombre("Mario");
			$per->obtenerNombre();


?>