<?php

/*CONDICIONES EN PHP
	If
*/

	if(8 == 7 or 5 == 6){
		
		echo "Es verdad";
	
	}elseif (5 == 5) {
		 //echo "La segunda condicion es verdadera";
	}
	else{
		echo "No es igual";
	}

/*BUCLE WHILE
*/
	
	$aumento = 5;

		while($aumento < 10)
		{
			//echo "<br/>".$aumento;
			$aumento++;
		}

//BUCLE DO WHILE
		//DO while permite realizar las acciones antes de realizar la condicion
		$variable1 = 10;

		do 
		{
			//echo $variable1;

		}while ($variable1 < 10); 

		//Diferencia con un bucle while(No se ejecuta)

		while($variable1 < 9)
		{
			//echo $variable1;
		}

//BUCLE FOR (Repetecion o logico)
		for ($i=5; $i>1; $i--) { 
				
			echo "<br/>".$i;					
		}

//BUCLE FOREACH
		//Nos permite traer todos los elementos y las claves de un array
		
		//Array predefinidos
		$predef = array("Elemento 1","Elemento 2");


		foreach ($predef as $key) 
		{
			echo "<br/>".$key;
		}

		echo "<br/>";

		//Array personalizado o asociativo
		$asoci = array("Clave1" => "Elemento_1","Clave2" => "Elemento_2");

		foreach ($asoci as $keys => $value) {
		
			echo "<br/>".$keys." = ".$value;
		}

//SWITCH
		//Permite evaluar el valor de una variable

	$var = 5;

	switch ($var) {
		case 1:
			echo "Vale 1";
			break;
		case 10:
			echo "<br/>"."Vale 10";
			break;
		case "Hola":
			echo "Es Hola";
			break;
		
		default:
			echo "<br/>"."Condicion no definida";
			break;
	}

?> 